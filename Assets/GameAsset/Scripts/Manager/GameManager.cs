using System;
using System.Collections.Generic;
using Base;
using Base.GameEventSystem;
using Base.Module;
using Base.Pattern;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Game
{
    public class GameManager : Singleton<GameManager>
    {
        private int _currentLevel = 1;
        private List<Choice> _choicesMade = new List<Choice>();
        private GameStateParam _gameStateParam;
        
        public static float PlayerBalance { get; set; }

        [SerializeField, CustomClassDrawer] private List<ItemPair> listGateItem;
        [SerializeField] private GameStateController gameStateController;
        [SerializeField] private GameEvent playStateNotify;
        [SerializeField] private GameEvent endStateNotify;

        public GameStateParam StateParam
        {
            get
            {
                if (_gameStateParam == null)
                {
                    _gameStateParam = new GameStateParam();
                }

                return _gameStateParam;
            }
        }

        public static int CurrentLevel => Instance._currentLevel;

        public static List<Choice> ChoicesMade => Instance._choicesMade;

        public static List<ItemPair> ListGateItem => Instance.listGateItem;

        private void Awake()
        {
            Application.targetFrameRate = 60;
            UnityEngine.Random.InitState(DateTime.Now.Millisecond);

            gameStateController.OnStateChanged += OnStateChanged;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            gameStateController.OnStateChanged -= OnStateChanged;
        }

        private void OnStateChanged(GameState from, GameState to)
        {
            if (to is InitializeState)
            {
                StateParam.IsWinOrLose = new Test(false, false);
            }
            else if (to is PlayingState)
            {
                playStateNotify.InvokeEvent();
            }
            else if (to is EndLevelState)
            {
                endStateNotify.InvokeEvent(new GameEventData(StateParam.IsWinOrLose.IsWin));
            }
        }

        public static void SetLevel(int value)
        {
            Instance.StateParam.ResultChoice = ResultChoice.None;
            Instance._currentLevel = value;
        }
    }

    public class GameStateParam
    {
        public Test IsWinOrLose = new Test(false, false);
        public ResultChoice ResultChoice = ResultChoice.None;
    }
    
    public enum Choice {None = 0, Bad, Good}
    
    public enum ResultChoice {None, Replay, Next}
}

