using System;
using System.Collections.Generic;
using System.Globalization;
using Base;
using Base.MessageSystem;
using FluffyUnderware.Curvy.Controllers;
using FluffyUnderware.Curvy.Generator;
using FluffyUnderware.Curvy.Generator.Modules;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class LevelManager : BaseMono
    {
        [SerializeField] private List<LevelController> listLevel;
        [SerializeField] private PlayerController playerController;
        [SerializeField] private VolumeController cameraVolume;

        [Header("UI"), SerializeField] private Image fillImage;
        [SerializeField] private Text levelText;

        [Space, SerializeField] private Skybox skybox;
        [SerializeField] private List<Material> skyboxMat;

        private float _totalFill = 0;

        public List<LevelController> ListLevel => listLevel;

        private void Start()
        {
            int crrLevel = Mathf.Clamp(GameManager.CurrentLevel, 1, listLevel.Count - 1);
            for (int i = 0; i < listLevel.Count; ++i)
            {
                if (i == crrLevel) listLevel[i].Active = true;
                else listLevel[i].Active = false;
            }
            var crrGenerator = listLevel[crrLevel].CurvyGenerator;
            var result = crrGenerator.Modules.Find(item => item is BuildShapeExtrusion);
            playerController.VolumeController.Volume = new CGDataReference(result, "Volume");
            cameraVolume.Volume = new CGDataReference(result, "Volume");

            var inputSpline = crrGenerator.GetComponentInChildren<InputSplinePath>();
            _totalFill = inputSpline.Spline.Length;
            levelText.text = $"{GameManager.CurrentLevel}";
            fillImage.fillAmount = 0;

            int skyboxIndex = GameManager.CurrentLevel % 3;
            skybox.material = skyboxMat[skyboxIndex];
        }

        private void LateUpdate()
        {
            var crrGenerator = listLevel[GameManager.CurrentLevel].CurvyGenerator;
            var inputSpline = crrGenerator.GetComponentInChildren<InputSplinePath>();
            var tf = inputSpline.Spline.GetNearestPointTF(playerController.Position);
            var distance = inputSpline.Spline.TFToDistance(tf);
            fillImage.fillAmount = distance / _totalFill;
        }
    }
}

