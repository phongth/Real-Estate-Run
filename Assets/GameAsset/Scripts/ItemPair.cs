using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    [Serializable]
    public class ItemPair
    {
        public Transform item;
        public Sprite itemSprite;
        public float correctPrice;
        public float wrongPrice;
        public ItemType itemType;
        public bool IsChosen { get; set; }
    }
    
    public enum ItemType {Car, Jewelry, Ship, House, Bag, Phone, Watch}
}

