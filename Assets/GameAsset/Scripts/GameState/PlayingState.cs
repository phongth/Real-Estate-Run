using System.Collections;
using System.Collections.Generic;
using Base.Pattern;
using UnityEngine;
using Base.MessageSystem;
using Base;

namespace Game
{
    public class PlayingState : GameState
    {
        public override void UpdateBehaviour(float dt)
        {
            if (GameStateController.InputAction.Phase != InputPhase.None)
            {
                Messenger.RaiseMessage(SystemMessage.Input, GameStateController.InputAction.Phase, GameStateController.InputAction.Position, GameStateController.InputAction.DeltaPosition);
            }
        }

        public override void CheckExitTransition()
        {
            if (GameManager.Instance.StateParam.IsWinOrLose.IsEndGame)
            {
                GameStateController.EnqueueTransition<EndLevelState>();
            }
        }
    }
}

