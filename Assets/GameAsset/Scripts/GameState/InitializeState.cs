using System.Collections;
using System.Collections.Generic;
using Base;
using Base.MessageSystem;
using Base.Pattern;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game
{
    public class InitializeState : GameState
    {
        private AsyncOperation _gameSceneOperation;

        private int _level = 1;
        public override void EnterStateBehaviour(float dt, GameState fromState)
        {
            SceneManager.LoadSceneAsync("UIScene", LoadSceneMode.Additive);
            _gameSceneOperation = SceneManager.LoadSceneAsync("Main Game Scene", LoadSceneMode.Additive);
            _gameSceneOperation.allowSceneActivation = false;
        }

        public override void CheckExitTransition()
        {
            if (_gameSceneOperation.progress >= .8f)
            {
                _gameSceneOperation.allowSceneActivation = true;
                GameStateController.EnqueueTransition<StartState>();
            }
        }

        public override void UpdateBehaviour(float dt)
        {
            
        }
    }
}

