using System.Collections;
using System.Collections.Generic;
using Base.Pattern;
using Base;
using Base.MessageSystem;
using UnityEngine;

namespace Game
{
    public class StartState : GameState
    {
        public override void UpdateBehaviour(float dt)
        {
            
        }

        public override void CheckExitTransition()
        {
            if (GameStateController.InputAction.Phase == InputPhase.Began)
            {
                GameStateController.EnqueueTransition<PlayingState>();
            }
        }
    }
}

