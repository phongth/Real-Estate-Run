using System.Collections;
using System.Collections.Generic;
using Base.Pattern;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game
{
    public class EndLevelState : GameState
    {
        private AsyncOperation _unloadOperation;

        public override void CheckExitTransition()
        {
            if (GameManager.Instance.StateParam.ResultChoice != ResultChoice.None)
            {
                GameStateController.EnqueueTransition<ReplayState>();
            }
        }

        public override void UpdateBehaviour(float dt)
        {
            
        }
    }
}

