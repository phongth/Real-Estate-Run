using System.Collections;
using System.Collections.Generic;
using Base.Pattern;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game
{
    public class ReplayState : GameState
    {
        private AsyncOperation _mainSceneOperation;
        public override void EnterStateBehaviour(float dt, GameState fromState)
        {
            base.EnterStateBehaviour(dt, fromState);

            SceneManager.UnloadSceneAsync("UIScene");
            _mainSceneOperation = SceneManager.UnloadSceneAsync("Main Game Scene");
            
            if (GameManager.Instance.StateParam.ResultChoice == ResultChoice.Next)
            {
                GameManager.SetLevel(GameManager.CurrentLevel + 1);
            }
            else GameManager.SetLevel(GameManager.CurrentLevel);
        }

        public override void CheckExitTransition()
        {
            if (_mainSceneOperation.progress >= .9f)
            {
                GameStateController.EnqueueTransition<InitializeState>();
            }
        }

        public override void UpdateBehaviour(float dt)
        {
            
        }
    }
}

