using System;
using Base;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Base.Module;

namespace Game
{
    public class GateController : BaseMono
    {
        [SerializeField] private TextMeshProUGUI choiceObjectPriceText;
        [SerializeField] private Image choiceObjectImage;
        [SerializeField] private GameObject blurObject;
        
        public bool IsCorrectGate { get; set; }

        public event Action<Collider, int> OnGateTrigger;

        public void SetGate(ItemPair item, GateType type, bool isCorrectGate = false)
        {
            IsCorrectGate = isCorrectGate;
            if (type == GateType.Item)
            {
                choiceObjectImage.gameObject.SetActive(false);
                if (isCorrectGate)
                {
                    if ((uint)Mathf.RoundToInt(item.correctPrice) / 1000000 == 0)
                    {
                        choiceObjectPriceText.text = $"${UtilsClass.FormatStringCommaSeparated((uint)Mathf.RoundToInt(item.correctPrice))}";
                    }
                    else choiceObjectPriceText.text = $"${UtilsClass.FormatMoney(Mathf.RoundToInt(item.correctPrice))}";
                }
                else
                {
                    if ((uint)Mathf.RoundToInt(item.correctPrice) / 1000000 == 0)
                    {
                        choiceObjectPriceText.text = $"${UtilsClass.FormatStringCommaSeparated((uint)Mathf.RoundToInt(item.wrongPrice))}";
                    }
                    else choiceObjectPriceText.text = $"${UtilsClass.FormatMoney(Mathf.RoundToInt(item.wrongPrice))}";
                }
            }
            else if (type == GateType.Price)
            {
                choiceObjectPriceText.gameObject.SetActive(false);
                if (isCorrectGate)
                    choiceObjectImage.sprite = item.itemSprite;
                else
                {
                    choiceObjectImage.sprite = item.itemSprite;
                }
            }
            else if (type == GateType.Cheaper || type == GateType.MoreExpensive)
            {
                choiceObjectPriceText.gameObject.SetActive(false);
                if (isCorrectGate)
                    choiceObjectImage.sprite = item.itemSprite;
                else
                {
                    choiceObjectImage.sprite = item.itemSprite;
                }
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                //Parent.gameObject.SetActive(false);
                choiceObjectImage.gameObject.SetActive(false);
                choiceObjectPriceText.gameObject.SetActive(false);
                OnGateTrigger?.Invoke(other, CacheTransform.GetSiblingIndex());
            }
        }
    }
}

