using System;
using Base;
using Base.GameEventSystem;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class MainUiController : BaseMono
    {
        [SerializeField] private GameObject tapToPlayTxt;
        [SerializeField] private GameObject bannerLose;
        [SerializeField] private GameObject bannerWin;
        [SerializeField] private GameObject nextButton;
        [SerializeField] private GameObject replayButton;
        [SerializeField] private Text playerBalance;

        private void Start()
        {
            bannerLose.SetActive(false);
            bannerWin.SetActive(false);
            nextButton.SetActive(false);
            replayButton.SetActive(false);

            playerBalance.text = "0";
        }

        private void LateUpdate()
        {
            // float crrBalance = float.Parse( playerBalance.text);
            // if (Mathf.Abs(crrBalance - GameManager.PlayerBalance) != 0)
            // {
            //     crrBalance = GameManager.PlayerBalance;
            //     playerBalance.text = $"{crrBalance}";
            // }
        }

        public void OnNextClick()
        {
            GameManager.Instance.StateParam.ResultChoice = ResultChoice.Next;
        }

        public void OnReplayClick()
        {
            GameManager.Instance.StateParam.ResultChoice = ResultChoice.Replay;
        }

        public void OnPlayStateNotify(GameEventData data)
        {
            tapToPlayTxt.SetActive(false);
        }

        public void OnEndStateNotify(GameEventData data)
        {
            bool isWin = (bool)data.Data;
            bannerLose.SetActive(!isWin);
            bannerWin.SetActive(isWin);
            nextButton.SetActive(isWin);
            replayButton.SetActive(!isWin);
            
            // float crrBalance = float.Parse( playerBalance.text);
            // if (Mathf.Abs(crrBalance - GameManager.PlayerBalance) != 0)
            // {
            //     crrBalance = GameManager.PlayerBalance;
            //     playerBalance.text = $"{crrBalance}";
            // }
        }
    }
}

