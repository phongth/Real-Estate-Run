using System;
using System.Collections;
using System.Collections.Generic;
using Base;
using UnityEngine;

namespace Game
{
    public struct CollectibleConstant
    {
        public const float Cash = 75f;
        public const float Bill = -150f;
        public const float Robber = -400f;
        public const float CorrectBonus = 150f;
        public const float WrongCost = 300f;
        public const float TotalBalanceToBillionaire = 5000;
    }
    
    public enum GameMessage {EndOfLine, FollowerEndOfLine}

    public class PlayerStateParam
    {
        public Test IsWinOrLose = new Test(false, false);
        public bool IsPlaying = false;
    }

    public class Test : Tuple<bool, bool>
    {
        public readonly bool IsEndGame;

        public readonly bool IsWin;
        public Test(bool item1, bool item2) : base(item1, item2)
        {
            IsEndGame = item1;
            IsWin = item2;
        }
    }
}

