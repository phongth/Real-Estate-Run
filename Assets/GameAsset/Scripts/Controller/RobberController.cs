using System;
using System.Collections;
using System.Collections.Generic;
using Base;
using Base.Module;
using FluffyUnderware.Curvy;
using FluffyUnderware.Curvy.Controllers;
using FluffyUnderware.Curvy.Generator;
using FluffyUnderware.Curvy.Generator.Modules;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Game
{
    public class RobberController : BaseMono
    {
        [SerializeField] private Transform rotateTransform;
        private VolumeController _volumeController;

        public Transform RotateTransform => rotateTransform;
        
        public bool IsHitPlayer { get; set; }
        private void Awake()
        {
            _volumeController = GetComponent<VolumeController>();
        }

        private void Start()
        {
            var curvyGenerator = this.GetComponentInBranch<CurvyGenerator>();
            if (curvyGenerator != null)
            {
                var cgModule = curvyGenerator.Modules.Find(item => item is BuildShapeExtrusion);
                _volumeController.Volume = new CGDataReference(cgModule, "Volume");
                var inputSpline = this.GetComponentInBranch<CurvyGenerator, InputSplinePath>();

                var tf = inputSpline.Spline.GetNearestPointTF(Position);
                var distance = inputSpline.Spline.TFToDistance(tf);
                _volumeController.Position = distance / inputSpline.Spline.Length;
            }
        }
    }
}

