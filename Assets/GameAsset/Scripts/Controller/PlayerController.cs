using System;
using System.Collections;
using System.Collections.Generic;
using Base;
using FluffyUnderware.Curvy;
using FluffyUnderware.Curvy.Controllers;
using Base.CharacterStats;
using Base.GameEventSystem;
using Base.MessageSystem;
using Base.Pattern;
using DG.Tweening;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Game
{
    public class PlayerController : BaseMono
    {
        [SerializeField] private CharacterStateController stateController;
        [SerializeField] private VolumeController volumeController;
        [SerializeField] private SplineController splineController;
        [SerializeField] private Transform rotateTransform;
        
        [SerializeField] private CharacterStat Money;
        [SerializeField] private RectTransform cashIn;
        [SerializeField] private RectTransform cashOut;
        
        [SerializeField, Space] private Transform collectibleMoneyPrefab;
        [SerializeField] private Transform moneyParent;
        [SerializeField] private List<Animator> animators;
        [SerializeField] private List<GameObject> chars = new List<GameObject>();
        [SerializeField] private ParticleSystem evolveFx;
        [SerializeField] private ParticleSystem devolveFx;
        [SerializeField] private Animator stateDriven;
        [SerializeField] private VolumeController drivenVolumeController;
        [SerializeField] private TextMeshProUGUI playerBalanceTxt;

        public LevelManager levelManager;

        public VolumeController VolumeController => volumeController;

        public SplineController SplineController => splineController;

        public Transform RotateTransform => rotateTransform;

        private PlayerStateParam _playerStateParam;

        private int _evolveStage = 0;

        private List<FollowerController> _listFollower = new List<FollowerController>();

        public CharacterStat Balance => Money;

        public PlayerStateParam StateParam
        {
            get
            {
                if (_playerStateParam == null) _playerStateParam = new PlayerStateParam();

                return _playerStateParam;
            }
        }

        private void Start()
        {
            Money = new CharacterStat(0);
            stateController.OnStateChanged += OnStateChanged;

            volumeController.Speed = 0;
            splineController.enabled = false;
        }

        private void Update()
        {
            if (volumeController.Position >= 1f)
            {
                StateParam.IsWinOrLose = new Test(true, true);
            }
        }

        private void LateUpdate()
        {
            if (Money.Value >= (moneyParent.childCount + 1) * 100f)
            {
                float[] a = new[] { -1f, 1f };
                int childCount = moneyParent.childCount;
                int b = childCount % 2;
                int c = childCount / 6;
                int e = (childCount) % 6;
                int d = Mathf.FloorToInt(e / 2f);
                Vector3 pos = new Vector3(0.5f * a[b], 0.25f * c, 0.5f * d);
                var money = Instantiate(collectibleMoneyPrefab, 
                    moneyParent.TransformPoint(new Vector3(0, 0.25f * childCount, 0)), Quaternion.identity, moneyParent);
                money.localRotation = Quaternion.identity;
            }
            else if (Money.Value <= (moneyParent.childCount - 1) * 100f)
            {
                int childCount = moneyParent.childCount;
                if (childCount > 0)
                {
                    Destroy(moneyParent.GetChild(childCount - 1).gameObject);
                }
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            stateController.OnStateChanged -= OnStateChanged;
        }

        private void OnStateChanged(CharacterState from, CharacterState to)
        {
            if (to is EndBehavior)
            {
                volumeController.enabled = false;
                splineController.enabled = true;
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Cash"))
            {
                Money.AddModifier(new StatModifier(CollectibleConstant.Cash, StatModType.PercentAdd, 0));
                var cashInGO = Instantiate(cashIn, cashIn.parent);
                cashInGO.position = cashIn.position;
                cashInGO.gameObject.SetActive(true);
                cashInGO.GetComponent<TextMeshProUGUI>().text = $"+{CollectibleConstant.Cash}$";
                cashInGO.DOAnchorPosY(0, .75f).SetEase(Ease.Linear).OnComplete(() =>
                {
                    cashInGO.anchoredPosition = new Vector2(cashIn.anchoredPosition.x, -1f);
                    Destroy(cashInGO.gameObject);
                });
            }
            else 
            {
                if (other.CompareTag("Bill"))
                {
                    Money.AddModifier(new StatModifier(CollectibleConstant.Bill, StatModType.PercentAdd, 1));
                    var cashOutGO = Instantiate(cashOut, cashOut.parent);
                    cashOutGO.position = cashOut.position;
                    cashOutGO.gameObject.SetActive(true);
                    cashOutGO.GetComponent<TextMeshProUGUI>().text = $"{CollectibleConstant.Bill}$";
                    cashOutGO.DOAnchorPosY(0, .75f).SetEase(Ease.Linear).OnComplete(() =>
                    {
                        cashOutGO.anchoredPosition = new Vector2(cashOut.anchoredPosition.x, -1f);
                        Destroy(cashOutGO.gameObject);
                    });
                }
                else if (other.CompareTag("Good Gate") || other.CompareTag("Bad Gate"))
                {
                    var groupGate = other.GetComponentInParent<GroupGateController>();
                    if (groupGate.CorrectGateTag.Equals(other.tag))
                    {
                        Money.AddModifier(new StatModifier(CollectibleConstant.CorrectBonus, StatModType.PercentAdd, 0));
                        // var cashInGO = Instantiate(cashIn, cashIn.parent);
                        // cashInGO.position = cashIn.position;
                        // cashInGO.gameObject.SetActive(true);
                        // cashInGO.GetComponent<TextMeshProUGUI>().text = $"+{CollectibleConstant.CorrectBonus}$";
                        // cashInGO.DOAnchorPosY(0, .75f).SetEase(Ease.Linear).OnComplete(() =>
                        // {
                        //     cashInGO.anchoredPosition = new Vector2(cashIn.anchoredPosition.x, -1f);
                        //     Destroy(cashInGO.gameObject);
                        // });
                        Evolution(1);
                    }
                    else
                    {
                        Money.AddModifier(new StatModifier(-1f * CollectibleConstant.WrongCost, StatModType.PercentAdd));
                        //
                        // var cashOutGO = Instantiate(cashOut, cashOut.parent);
                        // cashOutGO.position = cashOut.position;
                        // cashOutGO.gameObject.SetActive(true);
                        // cashOutGO.GetComponent<TextMeshProUGUI>().text = $"-{CollectibleConstant.WrongCost}$";
                        // cashOutGO.DOAnchorPosY(0, .75f).SetEase(Ease.Linear).OnComplete(() =>
                        // {
                        //     cashOutGO.anchoredPosition = new Vector2(cashOut.anchoredPosition.x, -1f);
                        //     Destroy(cashOutGO.gameObject);
                        // });
                        Evolution(-1);
                    }

                    // volumeController.Speed = 5;
                    // drivenVolumeController.Speed = 5;
                    // stateDriven.Play("Show Price");
                    // DelayAction(1f, () =>
                    // {
                    //     volumeController.Speed = 15;
                    //     drivenVolumeController.Speed = 15;
                    //     stateDriven.Play("Normal");
                    // }).Forget();
                }
                else if (other.CompareTag("Robber"))
                {
                    var robberController = other.GetComponent<RobberController>();
                    if (robberController && !robberController.IsHitPlayer)
                    {
                        Money.AddModifier(new StatModifier(CollectibleConstant.Robber, StatModType.PercentAdd));
                        var cashOutGO = Instantiate(cashOut, cashOut.parent);
                        cashOutGO.position = cashOut.position;
                        cashOutGO.gameObject.SetActive(true);
                        cashOutGO.GetComponent<TextMeshProUGUI>().text = $"{CollectibleConstant.Robber}$";
                        cashOutGO.DOAnchorPosY(0, .75f).SetEase(Ease.Linear).OnComplete(() =>
                        {
                            cashOutGO.anchoredPosition = new Vector2(cashOut.anchoredPosition.x, -1f);
                            Destroy(cashOutGO.gameObject);
                        });

                        volumeController.Speed = 0;
                        drivenVolumeController.Speed = 0;
                        
                        DelayAction(.75f, () =>
                        {
                            volumeController.Speed = 15;
                            drivenVolumeController.Speed = 15;
                        }).Forget();

                        robberController.IsHitPlayer = true;
                    }
                }

                if (Money.Value < 0)
                {
                    StateParam.IsWinOrLose = new Test(true, false);
                    drivenVolumeController.Speed = 0;
                }
            }
            
            playerBalanceTxt.text = $"{Money.Value}$";
        }

        private void Evolution(int i)
        {
            if (i > 0)
            {
                if (_evolveStage < animators.Count - 1)
                {
                    chars[_evolveStage].gameObject.SetActive(false);
                    _evolveStage += i;
                    chars[_evolveStage].gameObject.SetActive(true);
                    stateController.Animator = animators[_evolveStage];
                    //bags[_evolveStage].gameObject.SetActive(true);
                }
                
                evolveFx.Play(true);
            }
            else
            {
                if (_evolveStage + i >= 0)
                {
                    //bags[_evolveStage].gameObject.SetActive(false);
                    chars[_evolveStage].gameObject.SetActive(false);
                    _evolveStage += i;
                    chars[_evolveStage].gameObject.SetActive(true);
                    stateController.Animator = animators[_evolveStage];
                }
                devolveFx.Play(true);
            }
        }

        public void PlayVolumeController()
        {
            volumeController.Play();
            drivenVolumeController.Play();
        }

        public void OnPlayStateNotify(GameEventData data)
        {
            _playerStateParam.IsPlaying = true;
            drivenVolumeController.Speed = 18;
            volumeController.Speed = 18;
        }
    }
}

