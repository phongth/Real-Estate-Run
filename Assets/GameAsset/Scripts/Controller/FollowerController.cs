using System;
using System.Collections;
using System.Collections.Generic;
using Base;
using FluffyUnderware.Curvy.Controllers;
using UnityEngine;

namespace Game
{
    public class FollowerController : BaseMono
    {
        private VolumeController _volumeController;

        private VolumeController _followTarget;

        public VolumeController FollowTarget => _followTarget;

        private void Awake()
        {
            _volumeController = GetComponent<VolumeController>();
        }

        public void SetTarget(VolumeController target, float crossPosition = 0)
        {
            _followTarget = target;
            if (target != null)
            {
                _volumeController.Speed = 0;
                _volumeController.Volume = _followTarget.Volume;
                _volumeController.Position = _followTarget.Position - 0.005f;
                _volumeController.CrossRelativePosition = Mathf.Clamp(crossPosition, _volumeController.CrossFrom, _volumeController.CrossTo);
                _volumeController.Play();
            }
        }
    }
}

