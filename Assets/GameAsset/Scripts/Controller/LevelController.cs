using Base;
using FluffyUnderware.Curvy;
using FluffyUnderware.Curvy.Generator;
using FluffyUnderware.Curvy.Generator.Modules;
using TMPro;
using UnityEngine;
using System.Collections.Generic;

namespace Game
{
    public class LevelController : BaseMono
    {
        [SerializeField] private CurvyGenerator curvyGenerator;
        [SerializeField] private Transform emptyPlot;
        [SerializeField] private BuildVolumeSpots collectibleSpots;
        [SerializeField] private StairController stairToHeaven;
        [SerializeField] private List<TextMeshProUGUI> listText;

        public CurvyGenerator CurvyGenerator => curvyGenerator;

        public Transform EmptyPlot => emptyPlot;

        public CurvySpline Stair => stairToHeaven.GetPathToHeaven;

        public void ControlPointReached(int index)
        {
            listText[index - 1].color = Color.green;
        }
    }
}

