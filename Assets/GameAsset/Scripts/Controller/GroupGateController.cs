using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Base;
using Base.Module;
using DG.Tweening;
using SensorToolkit;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Game
{
    public class GroupGateController : BaseMono
    {
        [Header("Item Zone"), SerializeField] private GameObject itemZone;
        [SerializeField] private GameObject worldCanvas;
        [SerializeField] private Image goalObjectImage;
        [SerializeField] private TextMeshProUGUI goalObjectPrice;
        [SerializeField] private TextMeshProUGUI goalObjectPrice2;
        [Space, SerializeField] private GateController[] gates;
        [SerializeField, Header("Result Popup")] private GameObject resultPopup;
        [SerializeField] private GameObject correct;
        [SerializeField] private GameObject wrong;

        [SerializeField, Header("Show Question Popup")] private GameObject showQuestion;
        [SerializeField] private Image questionItem;
        [SerializeField] private Text questionPrice;
        

        private string _correctGateTag;

        private ItemPair _itemPair;

        private GateType _gateType;

        public string CorrectGateTag => _correctGateTag;
        public float WrongChoiceCost { get; private set; }

        private void Awake()
        {
            gates[0].OnGateTrigger += OnGateTrigger;
            gates[1].OnGateTrigger += OnGateTrigger;
        }

        private void Start()
        {
            int index = Parent.GetComponentsInChildren<GroupGateController>().ToList().IndexOf(this);

            var arr = new GateType[] { GateType.Item, GateType.Cheaper, GateType.MoreExpensive, GateType.Item, GateType.Cheaper };
            _gateType = arr[index];
            var cloneListGate = GameManager.ListGateItem.ToList();
            _itemPair = cloneListGate.FindRandomElement(item => !item.IsChosen);
            cloneListGate.Shuffle();
            _itemPair.IsChosen = true;
            var diffItem = cloneListGate.FindRandomElement(a => a.itemType != _itemPair.itemType && !a.IsChosen);
            WrongChoiceCost = Mathf.RoundToInt(_itemPair.wrongPrice);

            resultPopup.SetActive(false);
            showQuestion.SetActive(false);

            GateController correctGate = gates[Random.Range(0, gates.Length)];
            _correctGateTag = correctGate.tag;

            correct.GetComponent<Text>().text = $"Correct +${CollectibleConstant.CorrectBonus}";
            wrong.GetComponent<Text>().text = $"Wrong -${CollectibleConstant.WrongCost}";

            if (_gateType == GateType.Item)
            {
                correctGate.SetGate(_itemPair,_gateType, true);
                var wrongGate = gates.First(gate => gate != correctGate);
                wrongGate.SetGate(_itemPair, _gateType);
            }
            else if (_gateType == GateType.Cheaper)
            {
                if (_itemPair.correctPrice > diffItem.correctPrice)
                {
                    correctGate.SetGate(diffItem, _gateType, true);
                    var wrongGate = gates.First(gate => gate != correctGate);
                    wrongGate.SetGate(_itemPair, _gateType);
                }
                else
                {
                    correctGate.SetGate(_itemPair, _gateType, true);
                    var wrongGate = gates.First(gate => gate != correctGate);
                    wrongGate.SetGate(diffItem, _gateType);
                }
            }
            else if (_gateType == GateType.MoreExpensive)
            {
                if (_itemPair.correctPrice > diffItem.correctPrice)
                {
                    correctGate.SetGate(_itemPair, _gateType, true);
                    var wrongGate = gates.First(gate => gate != correctGate);
                    wrongGate.SetGate(diffItem, _gateType);
                }
                else
                {
                    correctGate.SetGate(diffItem, _gateType, true);
                    var wrongGate = gates.First(gate => gate != correctGate);
                    wrongGate.SetGate(_itemPair, _gateType);
                }
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            gates[0].OnGateTrigger -= OnGateTrigger;
            gates[1].OnGateTrigger -= OnGateTrigger;
        }

        private void OnGateTrigger(Collider other, int index)
        {
            gates[0].Active = false;
            gates[1].Active = false;
            itemZone.SetActive(false);
            worldCanvas.SetActive(false);
            
            showQuestion.SetActive(false);
            resultPopup.SetActive(true);

            if (gates[index].IsCorrectGate)
            {
                correct.SetActive(true);
                wrong.SetActive(false);
            }
            else
            {
                wrong.SetActive(true);
                correct.SetActive(false);
            }
            
            DelayAction(.75f, () => resultPopup.SetActive(false)).Forget();
        }

        public void OnPlayerDetected(GameObject obj, Sensor sensor)
        {
            showQuestion.SetActive(true);

            if (_gateType == GateType.Item)
            {
                questionPrice.gameObject.SetActive(false);
                questionItem.sprite = _itemPair.itemSprite;
            }
            else if (_gateType == GateType.Price)
            {
                questionItem.gameObject.SetActive(false);
                questionPrice.text = $"${Mathf.RoundToInt(_itemPair.correctPrice)}?";
            }
            else if (_gateType == GateType.Cheaper)
            {
                questionItem.gameObject.SetActive(false);
                questionPrice.gameObject.SetActive(true);
                questionPrice.text = "Cheaper?";
            }
            else if (_gateType == GateType.MoreExpensive)
            {
                questionItem.gameObject.SetActive(false);
                questionPrice.gameObject.SetActive(true);
                questionPrice.text = "More expensive?";
            }
        }
    }
    
    public enum GateType {Price, Item, Cheaper, MoreExpensive}
}

