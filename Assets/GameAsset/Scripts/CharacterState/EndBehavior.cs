using System.Collections;
using System.Collections.Generic;
using System;
using Base;
using Base.CharacterStats;
using Base.Module;
using Base.Pattern;
using FluffyUnderware.Curvy.Controllers;
using UnityEngine;

namespace Game
{
    public class EndBehavior : CharacterState
    {
        [SerializeField] private string endParam;
        [SerializeField] private Animator stateDrivenCam;
        [SerializeField] private SplineController endCash;
        [SerializeField] private Transform endCashParent;
        private PlayerController _playerController;

        private float _destination = 0;

        private float _multiply = 1;

        private float _offset = 0;

        private float _playerBalance = 0;

        private LevelController _level;

        protected override void Awake()
        {
            base.Awake();

            _playerController = this.GetComponentInBranch<PlayerController>();
        }

        public override void EnterState(float dt, CharacterState fromState)
        {
            _playerController.VolumeController.Pause();
            _playerBalance = _playerController.Balance.Value;
            if (_playerController.StateParam.IsWinOrLose.IsWin)
            {
                _level = _playerController.levelManager.ListLevel[GameManager.CurrentLevel];
                _playerController.SplineController.Spline = _level.Stair;
                float balance = Mathf.Clamp(_playerController.Balance.Value, 0, CollectibleConstant.TotalBalanceToBillionaire);
                float fragmentPos = Mathf.Round((balance / CollectibleConstant.TotalBalanceToBillionaire) * 10f) * 0.1f;
                int controlPointIndex = (int)(fragmentPos * _level.Stair.ControlPointCount) - 1;
                Vector3 position = _level.Stair.ControlPointsList[controlPointIndex].transform.localPosition;
                float nearestTf = _level.Stair.GetNearestPointTF(position);
                _destination = nearestTf;
                _offset = 100 * _destination / _playerController.Balance.Value;
                _playerController.SplineController.Play();
                stateDrivenCam.Play("To Heaven");
            }
            else
            {
                GameManager.Instance.StateParam.IsWinOrLose = new Test(true, false);
                GameManager.PlayerBalance = _playerBalance;
                _playerController.SplineController.Speed = 0;
            }
        }

        public override void UpdateBehaviour(float dt)
        {
            if (_playerController.SplineController.Position >= _offset * _multiply && _playerController.StateParam.IsWinOrLose.IsWin)
            {
                _playerController.Balance.AddModifier(new StatModifier(-100f, StatModType.PercentAdd));
                // var endCashSpline = Instantiate(endCash, Vector3.zero, Quaternion.identity, endCashParent);
                // endCashSpline.Spline = _playerController.SplineController.Spline;
                // endCashSpline.Position = _playerController.SplineController.Position;
                _multiply += 1;
            }
            if (_playerController.SplineController.Position >= _destination && !GameManager.Instance.StateParam.IsWinOrLose.IsEndGame)
            {
                _playerController.SplineController.Speed = 0;
                _playerController.SplineController.Pause();
                DelayAction(1.25f, () =>
                {
                    GameManager.Instance.StateParam.IsWinOrLose = new Test(true, true);
                    GameManager.PlayerBalance = _playerBalance;
                }).Forget();

                _playerController.RotateTransform.localRotation = Quaternion.Euler(0, 180, 0);
            }
        }

        public override void PostUpdateBehaviour(float dt)
        {
            if (CharacterStateController.Animator == null) return;
            if (!CharacterStateController.Animator.enabled) return;
            if (!CharacterStateController.Animator.gameObject.activeSelf) return;
            
            CharacterStateController.Animator.SetFloat("Speed", _playerController.SplineController.Speed);
            CharacterStateController.Animator.SetFloat(endParam, Convert.ToInt32(_playerController.StateParam.IsWinOrLose.IsWin) + 1);
        }

        public void OnControlPointReach(CurvySplineMoveEventArgs args)
        {
            int index = args.ControlPoint.transform.parent.IndexOf(args.ControlPoint.transform);
            _level.ControlPointReached(index);
        }
    }
}

