using System.Collections;
using System.Collections.Generic;
using Base.MessageSystem;
using Base.Module;
using Base.Pattern;
using FluffyUnderware.Curvy.Controllers;
using UnityEngine;

namespace Game
{
    public class NormalState_Follower : CharacterState
    {
        private VolumeController _volumeController;
        private FollowerController _follower;

        protected override void Start()
        {
            base.Start();

            _follower = this.GetComponentInBranch<FollowerController>();
            _volumeController = this.GetComponentInBranch<FollowerController, VolumeController>();
            
            Messenger.RegisterListener(GameMessage.EndOfLine, OnPlayerEndOfLine);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            
            Messenger.RemoveListener(GameMessage.EndOfLine, OnPlayerEndOfLine);
        }

        protected override void Update()
        {
            if (_follower.FollowTarget != null)
            {
                if (_follower.FollowTarget.PlayState == CurvyController.CurvyControllerState.Paused)
                {
                    _volumeController.Pause();
                }
                else
                {
                    //_volumeController.Stop();
                }

                _volumeController.Position = _follower.FollowTarget.Position - .005f;
            }
        }

        public override void CheckExitTransition()
        {
            
        }

        public override void UpdateBehaviour(float dt)
        {
            
        }

        private void OnPlayerEndOfLine()
        {
            
        }
    }
}

