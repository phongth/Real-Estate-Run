using System.Collections;
using System.Collections.Generic;
using Base;
using Base.MessageSystem;
using Base.Module;
using Base.Pattern;
using FluffyUnderware.Curvy;
using FluffyUnderware.Curvy.Controllers;
using UnityEngine;

namespace Game
{
    public class NormalMovement : CharacterState
    {
        [SerializeField] private float speedLerpRotate = .1f;
        [SerializeField] private string speedParam = "Velocity";
        
        private Vector3 _posClickOld;
        private Vector3 _deltaClick;
        private Vector3 _lookDir;
        private float _turnSmoothTime;
        
        private PlayerController _playerController;
        private float _crrLerpTime = 0;
        private float _lerpTime = 1f;

        protected override void Start()
        {
            base.Start();

            _playerController = this.GetComponentInBranch<PlayerController>();

            Messenger.RegisterListener<InputPhase, Vector3, Vector3>(SystemMessage.Input, OnInputResponse);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            
            Messenger.RemoveListener<InputPhase, Vector3, Vector3>(SystemMessage.Input, OnInputResponse);
        }

        public override void CheckExitTransition()
        {
            if (_playerController.StateParam.IsWinOrLose.IsEndGame)
            {
                CharacterStateController.EnqueueTransition<EndBehavior>();
            }
        }

        protected override void Update()
        {
            _crrLerpTime += Time.deltaTime;
            if (_crrLerpTime >= _lerpTime)
            {
                _crrLerpTime = 0;
            }
        }

        public override void UpdateBehaviour(float dt)
        {
            if (_playerController.StateParam.IsPlaying)
            {
                HandlingMovement(dt);
            }
        }

        public override void PostUpdateBehaviour(float dt)
        {
            if (CharacterStateController.Animator == null) return;
            if (!CharacterStateController.Animator.enabled) return;
            if (!CharacterStateController.Animator.gameObject.activeSelf) return;
            
            CharacterStateController.Animator.SetFloat(speedParam, _playerController.VolumeController.Speed);
            CharacterStateController.Animator.SetFloat("Balance", _playerController.Balance.Value);
        }

        private void OnInputResponse(InputPhase phase, Vector3 inputPos, Vector3 delta)
        {
            switch (phase)
            {
                case InputPhase.Began:
                    _posClickOld = inputPos;
                    break;
                case InputPhase.Moved:
                    _deltaClick = inputPos - _posClickOld;
                    _posClickOld = inputPos;
                    break;
                case InputPhase.Ended:
                    _deltaClick = Vector3.zero;
                    _lookDir = Vector3.zero;
                    break;
                case InputPhase.Stationary:
                    _deltaClick = Vector3.zero;
                    _lookDir = Vector3.zero;
                    break;
                default:
                    break;
            }
        }

        private void HandlingMovement(float dt)
        {
            if (_playerController.VolumeController)
            {
                var volume = _playerController.VolumeController;
                if (_playerController.VolumeController.PlayState != CurvyController.CurvyControllerState.Playing) _playerController.PlayVolumeController();
                // float speedRaw = volume.Speed + _deltaClick.y * dt;
                // volume.Speed = speedRaw;
                volume.CrossRelativePosition += .15f * _deltaClick.normalized.x * dt;

                if (_deltaClick != Vector3.zero)
                {
                    Vector3 deltaTouch = _deltaClick;
                    deltaTouch.z = deltaTouch.y;
                    deltaTouch.y = 0;

                    _lookDir += deltaTouch;
                    _lookDir = Vector3.ClampMagnitude(_lookDir, 150f);
                }

                if (_lookDir != Vector3.zero)
                {
                    float angle = Mathf.Atan2(_lookDir.x, _lookDir.z) * Mathf.Rad2Deg;
                    angle = Mathf.Clamp(angle, -90f, 90f);
                    //float smoothAngle = Mathf.SmoothDampAngle(transform.eulerAngles.y, angle, ref _turnSmoothTime, _crrLerpTime / _lerpTime);
                    //transform.rotation = Quaternion.Euler(0f, smoothAngle, 0);
                    _playerController.RotateTransform.localRotation = Quaternion.Lerp(_playerController.RotateTransform.localRotation, 
                        Quaternion.Euler(0f, angle, 0), speedLerpRotate);
                }
                else
                {
                    _playerController.RotateTransform.localRotation = Quaternion.Lerp(_playerController.RotateTransform.localRotation, 
                        Quaternion.Euler(0f, 0, 0), speedLerpRotate);
                }
            }
        }
    }
}

