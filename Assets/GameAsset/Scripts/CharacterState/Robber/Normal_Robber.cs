using System.Collections;
using System.Collections.Generic;
using Base.Module;
using Base.Pattern;
using FluffyUnderware.Curvy.Controllers;
using UnityEngine;

namespace Game
{
    public class Normal_Robber : CharacterState
    {
        private RobberController _robberController;
        private VolumeController _volumeController;
        private float _crossSpeed = 1f;
        private static readonly int Speed = Animator.StringToHash("Speed");

        protected override void Awake()
        {
            base.Awake();

            _robberController = this.GetComponentInBranch<RobberController>();
            _volumeController = this.GetComponentInBranch<RobberController, VolumeController>();
        }

        protected override void Start()
        {
            base.Start();
            float[] arr = { -1f, 1, -1f };
            int index = _robberController.CacheTransform.GetSiblingIndex();

            _crossSpeed = arr[index];
        }

        protected override void Update()
        {
            _volumeController.CrossRelativePosition += Time.deltaTime * .05f * _crossSpeed;
            _robberController.RotateTransform.localRotation = Quaternion.Euler(0, -90f * _crossSpeed, 0);
            if (_volumeController.CrossRelativePosition > _volumeController.CrossTo - .05f || _volumeController.CrossRelativePosition < _volumeController.CrossFrom + .05f)
            {
                _crossSpeed *= -1f;
            }
        }

        public override void UpdateBehaviour(float dt)
        {
            
        }

        public override void PostUpdateBehaviour(float dt)
        {
            if (CharacterStateController.Animator == null) return;
            if (!CharacterStateController.Animator.enabled) return;
            if (!CharacterStateController.Animator.gameObject.activeSelf) return;
            
            CharacterStateController.Animator.SetFloat(Speed, Mathf.Abs(_crossSpeed));
        }
    }
}

